﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris.Objects
{
    class TetriminoShapes
    {
        /*
         * Relative {x, y} coordinates of blocks in shapes.
         * Positive y is lower on screen.
         * { 0, 0 } is the rotation origin, and will not move after rotation.
         */
        static public int[,]
            // I-block
            I0 = { { -2,  0 }, { -1,  0 }, {  0,  0 }, {  1,  0 } },
            I1 = { {  0, -1 }, {  0,  0 }, {  0,  1 }, {  0,  2 } },
            // J-block
            J0 = { { -1,  0 }, {  0,  0 }, {  1,  0 }, {  1,  1 } },
            J1 = { { -1,  1 }, {  0, -1 }, {  0,  0 }, {  0,  1 } },
            J2 = { { -1,  1 }, {  0,  1 }, {  1,  1 }, { -1,  0 } },
            J3 = { {  0, -1 }, {  0,  0 }, {  0,  1 }, {  1, -1 } },
            // L-block
            L0 = { { -1,  0 }, {  0,  0 }, {  1,  0 }, { -1,  1 } },
            L1 = { {  0,  1 }, {  0,  0 }, {  0, -1 }, { -1, -1 } }, 
            L2 = { { -1,  1 }, {  0,  1 }, {  1,  1 }, {  1,  0 } }, 
            L3 = { {  1,  1 }, {  0,  1 }, {  0,  0 }, {  0, -1 } },
            // S-block
            S0 = { { -1,  1 }, {  0,  1 }, {  0,  0 }, {  1,  0 } },
            S1 = { { -1, -1 }, { -1,  0 }, {  0,  0 }, {  0,  1 } },
            // T-block
            T0 = { { -1,  0 }, {  0,  0 }, {  0,  1 }, {  1,  0 } },
            T1 = { {  1,  1 }, {  0,  0 }, {  1,  0 }, {  1, -1 } },
            T2 = { { -1,  1 }, {  0,  0 }, {  0,  1 }, {  1,  1 } },
            T3 = { { -1,  1 }, {  0,  0 }, { -1,  0 }, { -1, -1 } },
            // Z-block
            Z0 = { { -1,  0 }, {  0,  0 }, {  0,  1 }, {  1,  1 } },
            Z1 = { {  1, -1 }, {  1,  0 }, {  0,  0 }, {  0,  1 } },
            // O-block
            O0 = { {  0,  0 }, {  0,  1 }, {  1,  1 }, {  1,  0 } };

        /// <summary>
        ///   A dictionary where each value contains a list of all the shapes of the key
        /// </summary>
        public static Dictionary<string, List<int[,]>> Shapes = new Dictionary<string, List<int[,]>>()
        {
            { "O", new List<int[,]>(1) {O0} },
            { "I", new List<int[,]>(2) {I0, I1} },
            { "S", new List<int[,]>(2) {S0, S1} },
            { "Z", new List<int[,]>(2) {Z0, Z1} },
            { "J", new List<int[,]>(4) {J0, J1, J2, J3} },
            { "L", new List<int[,]>(4) {L0, L1, L2, L3} },
            { "T", new List<int[,]>(4) {T0, T1, T2, T3} },
        };

        /// <summary>
        ///     A dictionary telling which color a shape should be
        /// </summary>
        public static Dictionary<string, string> Colors = new Dictionary<string, string>()
        {
            { "O", "Blue" },
            { "I", "Red" },
            { "S", "Green" },
            { "Z", "Yellow" },
            { "J", "Silver" },
            { "L", "Pink" },
            { "T", "Brown" },
        };
    }
}