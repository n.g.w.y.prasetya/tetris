﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tetris.Objects
{
    class Block
    {
        private Playfield playfield;
        public Point fPosition;
        public Texture2D sprite;

        public Block(Playfield playfield, int x, int y, Texture2D sprite_)
        {
            this.playfield = playfield;
            fPosition = new Point(x, y);
            playfield.setPoint(fPosition, this);
            sprite = sprite_;
        }

        /// <summary>
        ///     Public Draw() method. Should be called every draw cycle.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="point"></param>
        public void Draw(SpriteBatch spriteBatch, Point point)
        {
            fPosition = point;
            //spriteBatch.Draw(sprite, Vector2.Zero, Color.White);
            spriteBatch.Draw(sprite, GetDrawingRectangle(point), Color.White);
        }

        public void Update(GameTime gameTime)
        {

        }

        /// <summary>
        ///     Gets the bounding box rectangle.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Rectangle GetDrawingRectangle(Point point)
        {
            return new Rectangle(GetScreenPos(point), Playfield.blockSize);
        }

        /// <summary>
        ///     Gets the screen coordinate of top-left corner of self.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Point GetScreenPos(Point point)
        {
            return (point.ToVector2() * Playfield.blockSize.X).ToPoint() + playfield.offsetLocation;
        }

        /// <summary>
        ///     Move self to point.
        /// </summary>
        /// <param name="point"></param>
        public void MoveTo(Point point)
        {
            playfield.movePoint(fPosition, point);
        }

        /// <summary>
        ///     Moves self relative to itself.
        /// </summary>
        /// <param name="point"></param>
        public void RelativeMoveTo(Point point)
        {
            playfield.relativeMovePoint(fPosition, point);
        }

        /// <summary>
        ///     Check if left from itself is empty.
        /// </summary>
        /// <returns></returns>
        public bool IsEmptyLeft()
        {
            return playfield.isPointEmpty(fPosition + new Point(-1, 0));
        }

        /// <summary>
        ///     Check if right from itself is empty.
        /// </summary>
        /// <returns></returns>
        public bool IsEmptyRight()
        {
            return playfield.isPointEmpty(fPosition + new Point(1, 0));
        }

        /// <summary>
        ///     Check if down from itself is empty.
        /// </summary>
        /// <returns></returns>
        public bool IsEmptyBelow()
        {
            return playfield.isPointEmpty(fPosition + new Point(0, 1));
        }
    }
}
