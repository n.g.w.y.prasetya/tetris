﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tetris.Managers.GameStates
{
    class StateGameOver : State
    {
        SpriteFont font;
        string score;

        public StateGameOver(string score)
        {
            font = Tetris.assetManager.GetFont("Font");
            this.score = score;
        }

        public override void Update(GameTime gameTime)
        {
            if (Tetris.inputManager.KeyPressed(Keys.Space))
            {
                Tetris.gameStateManager.Switch(GameStateManager.GameState.Game);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            string text =
                "GAME OVER - Score: " + score + "\n" +
                "Press space to try again.";
            spriteBatch.DrawString(font, text, Vector2.Zero, Color.Black);
        }
    }
}
