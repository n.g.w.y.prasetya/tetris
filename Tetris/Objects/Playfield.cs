﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Tetris.Objects;

namespace Tetris.Objects
{
    class Playfield
    {
        static public Point Size = new Point(12, 20);
        static public Point blockSize = new Point(20);
        static public Point smallBlock = new Point(10);

        public Point offsetLocation = Point.Zero;
        public Point pipeLocation = Point.Zero;

        public int speedLevel;
        public int subSpeedLevel;
        public double timeToNextFieldTick;
        public Score score;

        public Tetrimino tetrimino;
        public Block[,] field = new Block[Size.X, Size.Y];
        private Texture2D backGroundSprite;
        public string nextTetrimino = Tetrimino.GetRandomShapeName();

        public Playfield()
        {
            offsetLocation = new Point(100, 50);
            pipeLocation = new Point(offsetLocation.X + blockSize.X * 4, -blockSize.Y * 2);
            score = new Score(this);
            backGroundSprite = Tetris.assetManager.GetSprite("Block");
            field = new Block[Size.X, Size.Y];
            tetrimino = Tetrimino.NewRandomTetrimino(this);
            speedLevel = 1;
            timeToNextFieldTick = 1f / speedLevel;
        }

        /// <summary>
        ///     Increases subSpeedLevel. If over treshold, increase actual speedlevel.
        /// </summary>
        private void IncreaseDifficulty()
        {
            subSpeedLevel++;
            if (subSpeedLevel == 16)
            {
                subSpeedLevel = 0;
                speedLevel++;
            }
        }

        /// <summary>
        ///     Resets the time to next field tick.
        /// </summary>
        private void ResetTimeToNextFieldTick()
        {
            timeToNextFieldTick = 1f / speedLevel;
        }

        /// <summary>
        ///     Checks if a point is in the playfield.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool isInField(Point point)
        {
            return point.X >= 0 && point.X < Size.X && point.Y >= 0 && point.Y < Size.Y;
        }

        /// <summary>
        ///     Checks if a point is empty.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool isPointEmpty(Point point)
        {
            return field[point.X, point.Y] == null;
        }

        public Rectangle GetBoundingBox()
        {
            return new Rectangle(offsetLocation, Size * blockSize);
        }

        /// <summary>
        ///     Sets a point in the playfield to be a block.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="block"></param>
        public void setPoint(Point point, Block block)
        {
            field[point.X, point.Y] = block;
            field[point.X, point.Y].fPosition = point;
        }

        /// <summary>
        ///     Clears a point the playfield.
        /// </summary>
        /// <param name="point"></param>
        public void clearPoint(Point point)
        {
            field[point.X, point.Y] = null;
        }

        /// <summary>
        ///     Moves a block in the playfield relative to itself.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void relativeMovePoint(Point from, Point to)
        {
            movePoint(from, from + to);
        }

        /// <summary>
        ///     Moves a block in the playfield.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void movePoint(Point from, Point to)
        {
            if(to.Y >= Size.Y){
                field[from.X, from.Y] = null;
                return;
            }
            field[to.X, to.Y] = field[from.X, from.Y];
            field[from.X, from.Y] = null;
            field[to.X, to.Y].fPosition = to;
        }

        /// <summary>
        ///     Draw method which should be called every draw cycle.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            Random rng = new Random();
            for (int colum = 0; colum < field.GetLength(0); colum++)
            {
                for (int row = 0; row < field.GetLength(1); row++)
                {
                    Block block = field[colum, row];
                    if (block == null)
                    {
                        drawBackground(spriteBatch, colum, row);
                    }
                    else
                    {
                        block.Draw(spriteBatch, new Point(colum, row));
                    }
                }
            }
            drawPipe(spriteBatch);
            drawNextTetrimino(spriteBatch, nextTetrimino);

            score.Draw(spriteBatch);
        }

        /// <summary>
        ///     Draw a background sprite.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="colum"></param>
        /// <param name="row"></param>
        private void drawBackground(SpriteBatch spriteBatch, int colum, int row)
        {
            Point loc = new Point(colum * blockSize.X + offsetLocation.X, row * blockSize.Y + offsetLocation.Y);
            Rectangle drawingRectangle = new Rectangle(loc, blockSize);
            spriteBatch.Draw(backGroundSprite, drawingRectangle, Color.White);
        }

        private void drawPipe(SpriteBatch spriteBatch)
        {
            Rectangle drawRectangle = new Rectangle(pipeLocation, new Point (blockSize.X * 4,blockSize.Y * 5));
            Texture2D pipe = Tetris.assetManager.GetSprite("Pipe");
            spriteBatch.Draw(pipe, drawRectangle, Color.White); 
        }

        private void drawNextTetrimino(SpriteBatch spriteBatch, string nextTetrimino)
        {
            int[,] nextShape = TetriminoShapes.Shapes[nextTetrimino][0];
            for (int i = 0; i < 4; i++)
            {
                Point loc = new Point((nextShape[i, 0]) * smallBlock.X + pipeLocation.X + blockSize.X * 2, (nextShape[i, 1] - 4) * smallBlock.Y + offsetLocation.Y);
                Rectangle drawRectangle = new Rectangle(loc, smallBlock);
                if (nextTetrimino != null)
                {
                    Texture2D nextTetriminoSprite = Tetris.assetManager.GetSprite(TetriminoShapes.Colors[nextTetrimino]);
                    spriteBatch.Draw(nextTetriminoSprite, drawRectangle, Color.White);
                }
            }
        }

        /// <summary>
        ///     Destroys the tetrimino and create a new one.
        /// </summary>
        public void MergeTetrimino()
        {
            clearLines();
            if (isPointEmpty(new Point(6, 1)))
            {
                /*If you like: Invisible blocks after placement.
                foreach (Block block in tetrimino.blockList)
                {
                    block.sprite = Tetris.assetManager.GetSprite("block");
                }
                */

                // Reset the tetrimino
                tetrimino = Tetrimino.NewRandomTetrimino(this);
                IncreaseDifficulty();
            }
            else
            {
                tetrimino = null;
                Tetris.gameStateManager.Switch(Managers.GameStateManager.GameState.GameOver);
            }
        }

        /// <summary>
        ///     Clears the row, and moves the rows above down.
        /// </summary>
        /// <param name="rowToClear"></param>
        private void shiftFieldDown(int rowToClear)
        {
            for (int row = rowToClear; row >= 0 ; row--)
            {
                for (int colum = 0; colum < field.GetLength(0); colum++)
                {
                    // If the row is the row to clear, clear it.
                    if (row == rowToClear)
                    {
                        field[colum, row] = null;
                        // Go to next iteration of the loop.
                        continue;
                    }
                    // If point contains block, move it down.
                    field[colum, row]?.RelativeMoveTo(new Point(0, 1));
                }
            }
        }

        /// <summary>
        ///     Clears the full lines.
        /// </summary>
        private void clearLines()
        {
            byte linesCleared = 0;
            for (int row = 0; row < field.GetLength(1); row++)
            {
                bool lineIsFull = true;
                for (int colum = 0; colum < field.GetLength(0); colum++)
                {
                    // If row contains empty spot, go to next row.
                    if (isPointEmpty(new Point(colum, row)))
                    {
                        lineIsFull = false;
                        break;
                    }
                }
                // If row is full, shift field and increase linesCleared.
                if (lineIsFull)
                {
                    Tetris.assetManager.PlaySound("Line");//plays louder if multiple lines are cleared.
                    shiftFieldDown(row);
                    linesCleared++;
                }
            }
            score.AwardLineClear(linesCleared);
        }

        /// <summary>
        ///     Update method. Should be called every game update.
        /// </summary>
        /// <param name="gametime"></param>
        public void Update(GameTime gametime)
        {
            tetrimino?.Update(gametime);
            double elpasedTime = gametime.ElapsedGameTime.TotalSeconds;

            if(timeToNextFieldTick < 0)
            {
                FieldTick();
                ResetTimeToNextFieldTick();
            }
            else
            {
                timeToNextFieldTick -= elpasedTime;
            }
        }

        /// <summary>
        ///     Ticks slower than Update(), but goes faster as difficulty increases.
        /// </summary>
        private void FieldTick()
        {
            // If tetrimino is not null, try to move it down.
            tetrimino?.moveDown();
        }
    }
}
