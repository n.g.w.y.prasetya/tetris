﻿/* 
 * AssetManager van de voorbeeld programma's uit het boek:
 * Learning C# by Programming Games door Springer
 */

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;


public class AssetManager
{
    public bool muted = false;
    protected ContentManager contentManager;

    public AssetManager(ContentManager content)
    {
        contentManager = content;
    }

    public Texture2D GetSprite(string assetName)
    {
        if (assetName == "")
        {
            return null;
        }
        return contentManager.Load<Texture2D>(assetName);
    }

    public void Mute()
    {
        if (MediaPlayer.Volume == 1f)
        {
            MediaPlayer.Volume = 0f;
            muted = true;
        } else
        {
            MediaPlayer.Volume = 1f;
            muted = false;
        }

    }

    public SpriteFont GetFont(string assetName)
    {
        if (assetName == "")
        {
            return null;
        }
        return contentManager.Load<SpriteFont>(assetName);
    }

    public void PlaySound(string assetName)
    {
        if (muted == false)
        {
            SoundEffect snd = contentManager.Load<SoundEffect>(assetName);
            snd.Play();
        }
    }

    public void PlayMusic(string assetName, bool repeat = true)
    {
        MediaPlayer.IsRepeating = repeat;
        MediaPlayer.Play(contentManager.Load<Song>(assetName));
    }

    public ContentManager Content
    {
        get { return contentManager; }
    }
}