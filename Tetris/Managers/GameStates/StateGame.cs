﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tetris.Objects;
using Tetris.Managers;

namespace Tetris.Managers.GameStates
{
    class StateGame : State
    {
        public Playfield playfield;

        public StateGame()
        {
            playfield = new Playfield();
        }

        public override void Update(GameTime gameTime)
        {
            playfield.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            playfield.Draw(spriteBatch);
        }
    }
}
