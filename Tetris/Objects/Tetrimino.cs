﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Tetris.Objects;

namespace Tetris.Objects
{
    //Dit is het figuur!
    class Tetrimino
    {
        private Playfield playfield;
        public List<Block> blockList;
        public Texture2D sprite;
        public Point fPosition = new Point(Playfield.Size.X / 2, 0);
        public int rotation = 0;
        public string shape;

        public Tetrimino(Playfield playfield)
        {
            this.playfield = playfield;
        }

        public Tetrimino(Playfield playfield, string shape)
        {
            this.playfield = playfield;
            this.shape = shape;
            sprite = Tetris.assetManager.GetSprite(TetriminoShapes.Colors[shape]);
            LoadShape(TetriminoShapes.Shapes[shape][0], sprite);
        }

        static public Tetrimino NewRandomTetrimino(Playfield pf)
        {
            Tetrimino newTetrimino = new Tetrimino(pf, pf.nextTetrimino);
            pf.nextTetrimino = GetRandomShapeName();
            Tetris.assetManager.PlaySound("PipeSnd");
            return newTetrimino;
        }

        static public string GetRandomShapeName()
        {
            Random rng = new Random();
            int n = rng.Next(0, TetriminoShapes.Shapes.Keys.Count);
            return TetriminoShapes.Shapes.Keys.ElementAt(n);
        }

        public void LoadShape(int[,] shape, Texture2D sprite)
        {
            if(blockList != null)
            {
                for (int i = 0; i < blockList.Count; i++)
                {
                    Point toReset = blockList[i].fPosition;
                    playfield.clearPoint(toReset);
                }
            }
            blockList = new List<Block>(4);
            for (int i = 0; i < shape.GetLength(0); i++)
            {
                Block block = new Block(playfield, shape[i, 0] + fPosition.X, shape[i, 1] + fPosition.Y, sprite);
                blockList.Add(block);
            }
        }

        /// <summary>
        ///     If possible, rotates tetrimino.
        /// </summary>
        public void Rotate()
        {
            switch (TetriminoShapes.Shapes[shape].Count)
            {
                case 1:
                    break;
                case 2:
                    if (rotation == 0)
                    {
                        tryRotateTo(1);
                    }
                    else
                    {
                        tryRotateTo(0);
                    }
                    break;
                case 4:
                    if (rotation == 3)
                    {
                        tryRotateTo(0);
                    }
                    else
                    {
                        tryRotateTo(rotation + 1);
                    }
                    break;
            }
        }

        /// <summary>
        ///     If possible, rotate the tetrimino.
        /// </summary>
        /// <param name="rotation"></param>
        private void tryRotateTo(int rotation)
        {
            if (canFitShape(TetriminoShapes.Shapes[shape][rotation]))
            {
                LoadShape(TetriminoShapes.Shapes[shape][rotation], sprite);
                this.rotation = rotation;
            }
        }

        /// <summary>
        ///     Checks if the tetrimino can morph to a shape.
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        public bool canFitShape(int [,] shape)
        {
            bool canFit = true;
            for (int i = 0; i < shape.GetLength(0); i++)
            {
                Point relativeP = new Point(shape[i, 0], shape[i, 1]);
                Point toCheck = fPosition + relativeP;
                Console.WriteLine("Checking: " + toCheck);

                Block tBlock = blockList.Find(tBlock_ => tBlock_.fPosition == toCheck);
                if (!playfield.isInField(toCheck) || (tBlock == null && !playfield.isPointEmpty(toCheck)))
                {
                    canFit = false;
                    break;
                }
            }
            return canFit;
        }

        private int sortForMoveDown(Block a, Block b)
        {
            return b.fPosition.Y.CompareTo(a.fPosition.Y);
        }

        private int sortForMoveRight(Block a, Block b)
        {
            return b.fPosition.X.CompareTo(a.fPosition.X);
        }

        private int sortForMoveLeft(Block a, Block b)
        {
            return a.fPosition.X.CompareTo(b.fPosition.X);
        }
        
        /// <summary>
        ///     If possible, move left.
        /// </summary>
        public void moveLeft()
        {
            if (CanMoveLeft())
            {
                blockList.Sort(sortForMoveLeft);
                fPosition.X--;
                foreach (Block block in blockList)
                {
                    block.RelativeMoveTo(new Point(-1, 0));
                }
            }
        }

        /// <summary>
        ///     If possible, move right.
        /// </summary>
        public void moveRight()
        {
            if (CanMoveRight())
            {
                blockList.Sort(sortForMoveRight);
                fPosition.X++;
                foreach (Block block in blockList)
                {
                    block.RelativeMoveTo(new Point(1, 0));
                }
            }
        }

        /// <summary>
        ///     If possible, move down. Else, merges tetrimino. If forced, award points for softdrop.
        /// </summary>
        /// <param name="forced">True if softdropping. Defaults to false.</param>
        public void moveDown(bool forced = false)
        {
            if (CanMoveDown())
            {
                if (forced)
                {
                    playfield.score.IncrementSoftDrop();
                }

                blockList.Sort(sortForMoveDown);
                fPosition.Y++;

                foreach (Block block in blockList)
                {
                    block.RelativeMoveTo(new Point(0, 1));
                }
            }
            else
            {
                playfield.MergeTetrimino();
            }
        }
        
        /// <summary>
        ///     Check if the tetrimino can move left.
        /// </summary>
        /// <returns></returns>
        public bool CanMoveLeft()
        {
            foreach (Block block in blockList)
            {
                if (block.fPosition.X == 0)
                {
                    return false;
                }

                // Check if block left is not in blockList
                Block tBlock = blockList.Find(tBlock_ => tBlock_.fPosition == block.fPosition + new Point(-1, 0));
                // If block is not in blockList, and not empty, return false.
                if (tBlock == null && !block.IsEmptyLeft())
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        ///     Check if the tetrimino can move right.
        /// </summary>
        /// <returns></returns>
        public bool CanMoveRight()
        {
            foreach (Block block in blockList)
            {
                if (block.fPosition.X == Playfield.Size.X - 1)
                {
                    return false;
                }

                // Check if block left is not in blockList
                Block tBlock = blockList.Find(tBlock_ => tBlock_.fPosition == block.fPosition + new Point(1, 0));
                // If block is not in blockList, and not empty, return false.
                if (tBlock == null && !block.IsEmptyRight())
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        ///     Checks if the tetrimino can move down.
        /// </summary>
        /// <returns></returns>
        public bool CanMoveDown()
        {
            foreach (Block block in blockList)
            {
                if(block.fPosition.Y == Playfield.Size.Y - 1)
                {
                    return false;
                }

                // Check if block below is not in blockList
                Block tBlock = blockList.Find(tBlock_ => tBlock_.fPosition == block.fPosition + new Point(0, 1));
                // If block is not in blockList, and not empty, return false.
                if (tBlock == null && !block.IsEmptyBelow())
                {
                    return false;
                }
            }
            return true;
        }

        public void Update(GameTime gametime)
        {
            if (Tetris.inputManager.KeyPressed(Keys.M, false))
            {
                Tetris.assetManager.Mute();
            }

            if (Tetris.inputManager.KeyPressed(Keys.Up, false))
            {
                Rotate();
            }

            if (Tetris.inputManager.KeyPressed(Keys.Down))
            {
                moveDown(true);
            }

            if (Tetris.inputManager.KeyPressed(Keys.Left))
            {
                moveLeft();
                return;
            }

            if (Tetris.inputManager.KeyPressed(Keys.Right))
            {
                moveRight();
                return;
            }
        }
    }
}
