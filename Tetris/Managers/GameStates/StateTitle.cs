﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tetris.Managers.GameStates
{
    class StateTitle : State
    {
        SpriteFont font;
        
        public StateTitle()
        {
            font = Tetris.assetManager.GetFont("Font");
        }

        public override void Update(GameTime gameTime)
        {
            if (Tetris.inputManager.KeyPressed(Keys.Space))
            {
                Tetris.gameStateManager.Switch(GameStateManager.GameState.Game);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, "Press space to continue.", Vector2.Zero, Color.Black);
        }
    }
}
