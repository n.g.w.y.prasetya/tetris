﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tetris.Objects
{
    class Score
    {
        int score;
        int softDropScore;
        int combo;
        int maxLength;
        Playfield playfield;
        SpriteFont font = Tetris.assetManager.GetFont("Font");

        public Score(Playfield playfield)
        {
            maxLength = 12;
            score = 0;
            softDropScore = 0;
            combo = 1;
            this.playfield = playfield;
        }

        /// <summary>
        ///     Draw the score on screen.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, ToString(), new Vector2(playfield.GetBoundingBox().Left, playfield.GetBoundingBox().Bottom), Color.Black);
        }

        /// <summary>
        ///     Converts the score int to a string and suffixes 2 zeroes.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string s = score.ToString();
            return new string('0', maxLength - 2 - s.Length) + s + "00";
        }

        /// <summary>
        ///     Awards scores on line cleared.
        /// </summary>
        /// <param name="linesCleared"></param>
        public void AwardLineClear(byte linesCleared)
        {
            UpdateCombo(linesCleared);
            // Scoring formula accoring to: http://tetris.wikia.com/wiki/Tetris_The_Grand_Master
            score += (int)Math.Ceiling(playfield.speedLevel + linesCleared / 4f + softDropScore) * linesCleared * combo;
            softDropScore = 0;
        }

        /// <summary>
        ///     Increases the potential score from dropping a tetrimino.
        /// </summary>
        public void IncrementSoftDrop()
        {
            softDropScore++;
        }

        /// <summary>
        ///     Updates the combo.
        /// </summary>
        /// <param name="linesCleared"></param>
        private void UpdateCombo(byte linesCleared)
        {
            if (linesCleared == 0)
            {
                combo = 1;
                return;
            }
            // Combo formula accoring to: http://tetris.wikia.com/wiki/Tetris_The_Grand_Master
            combo += linesCleared * 2 - 2;
            if (combo <= 0)
            {
                combo = 1;
            }
        }
    }
}
