﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Managers.GameStates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tetris.Managers
{
    public class GameStateManager
    {
        public GameState gameState;
        private State state;

        public GameStateManager()
        {
            gameState = GameState.TitleScreen;
        }

        public enum GameState
        {
            TitleScreen, Game, GameOver
        }

        public void Switch(GameState to)
        {
            switch (to)
            {
                case GameState.Game:
                    state = new StateGame();
                    break;
                case GameState.TitleScreen:
                    state = new StateTitle();
                    break;
                case GameState.GameOver:
                    state = new StateGameOver((state as StateGame).playfield.score.ToString());
                    break;
            }
            gameState = to;
        }

        public void Update(GameTime gameTime)
        {
            state?.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            state?.Draw(spriteBatch);
        }
    }
}
